# Spark Library example
This is an example of one possible way of structuring a Spark application, forked from
one of the original examples at https://sparktutorials.github.io/.

See also http://sparkjava.com/.

## Make it work
To run the application on your machine:

```
cd spark-basic-structure
mvn clean compile assembly:single
java -jar .\target\spark-basic-structure-1.0-jar-with-dependencies.jar
```

Then go to http://localhost:8008/index/. 
To log in, see account details on the index page.

## Docker
The Dockerfile is aimed at a RaspberryPi architecture. Change the first line to
match your architecture. See https://hub.docker.com/.

When you have docker installed: 

```
docker build -t spark/library https://rschellius@bitbucket.org/AEI-informatica/spark-library-example.git
docker run -p 8081:8008 -d spark/library	// run as deamon on external port 8081
docker run --restart=unless-stopped -p 8081:8008 -d spark/library
docker ps -a 								// shows the container ID
docker exec -it <container ID> /bin/bash	// connects a shell
```

See a running example on http://home.level-ict.nl:8081/index/.

## Critique welcome
If you find anything you disagree with, please feel free to create an issue.

